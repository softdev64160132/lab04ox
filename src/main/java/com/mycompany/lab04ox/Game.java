/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04ox;

import java.util.Scanner;

/**
 *
 * @author asus
 */
public class Game {

    private Player player1, player2;
    private Table table;

    public Game() {
        player1 = new Player('x');
        player2 = new Player('o');
    }

    public void play() {
        printWelcome();
        newGame();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();
            if (table.checkWin() == true) {
                printTable();
                printWinner();
                printPlayers();
                if (playAgain() == false) {
                    printEnded();
                    break;
                } else {
                    table.resetGame();
                }
            } else if (table.checkDraw() == true) {
                printTable();
                printDraw();
                printPlayers();
                if (playAgain() == false) {
                    printEnded();
                    break;
                } else {
                    table.resetGame();
                }
            }
            table.switchPlayer();

        }
    }

    private void printWelcome() {
        System.out.println("=== Welcome to OX game ===");
    }

    private void printTable() {
        char[][] tb = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print("|" + tb[i][j] + "|");
            }
            System.out.println("");
        }
    }

    private void printTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn!!");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row col :");
        int row = 0;
        int col = 0;

        while (true) {
            row = sc.nextInt();
            col = sc.nextInt();
            if (row <= 3 && col <= 3) {
                
                table.setRowCol(row, col);
                break;
            }else {
                System.out.print("Please input row col between (1-3) : ");
            }
        }

    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void printWinner() {
        System.out.println(">> The winner is " + table.getCurrentPlayer().getSymbol() + "!!");
    }

    private void printDraw() {
        System.out.println(">> This game is Draw !!");
    }

    private void printPlayers() {
        System.out.println(player1);
        System.out.println(player2);
    }

    private boolean playAgain() {
        System.out.println("Do you want to play again? (y/n)");
        Scanner sc = new Scanner(System.in);
        String message = sc.next();
        return message.equals("y");
    }

    private void printEnded() {
        System.out.println("=== End game ===");
    }


}
